module "networking" {
  source    = "./modules/networking"
  namespace = var.namespace
}

module "ssh-gen" {
  source    = "./modules/ssh-gen"
  namespace = var.namespace
}
module "ec2" {
  source    = "./modules/ec2"
  namespace = var.namespace
  vpc       = module.networking.vpc
  sg_pub_id = module.networking.sg_pub_id
  sg_pri_id = module.networking.sg_pri_id
  key_name  = module.ssh-gen.key_name
}
module "s3" {
  source    = "./modules/s3"
  namespace = var.namespace
  key_name = module.ssh-gen.key_name
}